<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2023 Dallas Strouse -->
<component type="desktop">
  <id>@app-id@</id>
  <launchable type="desktop-id">@app-id@.desktop</launchable>
  <name>Toggle</name>
  <summary>Toggle extra GNOME settings</summary>
  <!-- IMPORTANT: Change these to match the actual license of your app-->
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0</project_license>
  <developer_name>Dallas Strouse</developer_name>
  <update_contact>dastrouses@gmail.com</update_contact>
  <description>
    <p>Toggle empowers you to customize additional GNOME configurations beyond the scope of GNOME Settings, catering to:</p>
    <ul>
      <li>Experimental features</li>
      <li>Advanced accessibility adjustments</li>
      <li>Modifications to the GNOME workflow</li>
    </ul>
  </description>
  <screenshots>
    <screenshot type="default">
      <image>
        https://gitlab.gnome.org/World/toggle/raw/HEAD/data/screenshots/screenshot-1.png</image>
    </screenshot>
    <screenshot>
      <image>
        https://gitlab.gnome.org/World/toggle/raw/HEAD/data/screenshots/screenshot-2.png</image>
    </screenshot>
  </screenshots>
  <!-- IMPORTANT: Use https://hughsie.github.io/oars/generate.html to generate before publishing
  your app -->
  <content_rating type="oars-1.1" />
  <releases>
    <release version="45.0-1" date="2023-11-18" />
  </releases>
  <url type="homepage">https://gitlab.gnome.org/World/toggle</url>
  <url type="bugtracker">https://gitlab.gnome.org/World/toggle/-/issues/new</url>
  <url type="help">https://matrix.to/#/#toggle-application:fedora.im</url>
</component>
