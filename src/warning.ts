import Adw from "gi://Adw";
import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";

export class ToggleWarningWindow extends Adw.Bin {
  private _picture!: Gtk.Picture;

  static {
    GObject.registerClass(
      {
        GTypeName: "ToggleWarningWindow",
        Template: "resource:///app/drey/Toggle/ui/warning.ui",
        InternalChildren: ["picture"],
        Signals: {
          warning_accepted: {},
        },
      },
      this
    );
  }

  constructor(params?: Partial<Adw.Bin.ConstructorProperties>) {
    super(params);

    this._picture.set_resource(
      "/app/drey/Toggle/illustrations/unsupported-options.svg"
    );
  }

  private warning_accepted_cb() {
    this.emit("warning-accepted");
  }
}
